# HLA Shared Copula

Research into whether the HLA haplotype copula is shared among ethnicities and the optimal effective sample size for Bayesian estimation. For publication in Experimental Results.

## Code

The code used to derive the results is available in [code/main.ipynb](code/main.ipynb). The code to generate the figure is in [code/plots.ipynb](code/plots.ipynb)

## Manuscript

The LaTeX used to engrave the paper and a PDF version are available in [manuscript/](manuscript/).